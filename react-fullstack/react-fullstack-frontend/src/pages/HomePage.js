import React from 'react'

const HomePage = () => (
    <>
        <h1>Hello, welcome to my blog</h1>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod condimentum leo et tristique.
            Etiam luctus cursus eleifend. Sed egestas dolor vel turpis euismod ultricies.
            Pellentesque pellentesque dolor in placerat consequat. Suspendisse quis sem nec tellus pellentesque accumsan.
            Curabitur hendrerit velit ac dictum dictum. Sed vitae venenatis neque. Vestibulum tristique ipsum odio, a eleifend nibh viverra vel.
            Donec id sem nec risus tempor eleifend. Sed elementum imperdiet pharetra. Nulla facilisi.
        </p>
        <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque euismod condimentum leo et tristique.
            Etiam luctus cursus eleifend. Sed egestas dolor vel turpis euismod ultricies.
            Pellentesque pellentesque dolor in placerat consequat. Suspendisse quis sem nec tellus pellentesque accumsan.
            Curabitur hendrerit velit ac dictum dictum. Sed vitae venenatis neque. Vestibulum tristique ipsum odio, a eleifend nibh viverra vel.
            Donec id sem nec risus tempor eleifend. Sed elementum imperdiet pharetra. Nulla facilisi.
        </p>
    </>
)

export default HomePage;