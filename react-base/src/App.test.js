import { render } from "@testing-library/react"
import React from "react"
import App from "./App"

test("renders and h1", () => {
    const { getByText } = render(<App />);
    const h1 = getByText(/Hello/);
    expect(h1).toHaveTextContent(
        "Hello"
    );
});