import React from "react"
import { Checkbox } from "./Checkbox"
import { fireEvent, render } from "@testing-library/react";


test("Selecting checkbox", () => {
    const { getByLabelText } = render(<Checkbox />);
    const checkBox = getByLabelText(/not checked/);
    fireEvent.click(checkBox);
    expect(checkBox.checked).toEqual(true)

})